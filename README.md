# ectomigo: personal edition

[ectomigo](https://ectomigo.com) is an organizational defense against the risks of schema migration, continuously scanning data access code and raising alerts when database changes submitted for review could break existing systems.

ectomigo personal edition turns the same scanning engine into a reference tool for individual developers. If you're wondering what the blast radius of a prospective change _might_ be, `ectomigo check tablename` will surface every mention of it in every codebase you've scanned.

```
npm i -g @ectomigo/ectomigo
```

or, to install for just yourself (make sure ~/.local/bin is on your `PATH`!):

```
npm i -g @ectomigo/ectomigo --prefix ~/.local
```

## usage

Once installed you will have an `ectomigo` executable on your `PATH`. Run it without any arguments to see help, or run `ectomigo help <command>` to see detailed instructions for subcommands.

You must run `ectomigo init` first to set up your database. After that, the most important commands are `ectomigo scan path/to/code` and `ectomigo check <entity name>`.

## requirements

* PostgreSQL 13+
* NodeJS 16+
