# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/ectomigo/ectomigo/compare/v0.0.2...v0.0.3) (2022-04-28)


### Bug Fixes

* clean init ([fa2737a](https://gitlab.com/ectomigo/ectomigo/commit/fa2737a393f2bb71b492fa8bde95c40efa9ef4eb))

### 0.0.2 (2022-04-28)


### Features

* ectomigo personal edition ([19cfd19](https://gitlab.com/ectomigo/ectomigo/commit/19cfd19680c43be83239f75db410708678a9e6b8))
