#!/usr/bin/env node

'use strict';

import _ from 'lodash';
import massive from 'massive';
import {program} from 'commander';
import indexFiles from '@ectomigo/core/lib/indexer/files.js';
import {globby} from 'globby';
import {basename, resolve} from 'path';
import {Readable, Writable} from 'stream';
import {pipeline} from 'stream/promises';
import colors from '@colors/colors/safe.js';

const DEFAULT_IGNORE = ['tags'];
const DB_NAME = 'ectomigo';

program
  .command('init')
  .option('-y, --yes-i-acknowledge-this-resets-the-database', 'if you have an existing ectomigo database, drop it first or pass this flag')
  .description('initialize a local ectomigo database')
  .action(async function (options) {
    if (!options.yesIAcknowledgeThisResetsTheDatabase) {
      try {
        const testDb = await massive(DB_NAME);

        console.log(colors.brightRed('ectomigo database already exists! Refusing to drop existing database.'));
        console.log();
        console.log('Drop your ectomigo database or pass -y to ectomigo init.');

        process.exit(1);
      } catch (ignored) {}
    }

    const manageDb = await massive('postgres');

    await manageDb.query(`DROP DATABASE IF EXISTS ${DB_NAME}`);
    await manageDb.query(`CREATE DATABASE ${DB_NAME}`);

    const initDb = await massive(DB_NAME);

    await initDb.query(`
      CREATE TABLE repos (
        repo_id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
        name text NOT NULL,
        url text NOT NULL,
        patterns jsonb,
        ignore_paths text[],
        created_at timestamp with time zone DEFAULT now() NOT NULL,
        scanned_at timestamp with time zone
      )`);

    await initDb.query(`
       CREATE TABLE invocations (
        invocation_id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
        repo_id uuid NOT NULL REFERENCES repos (repo_id) ON DELETE CASCADE,
        file_path text NOT NULL,
        confidence real DEFAULT 1 NOT NULL,
        x1 integer NOT NULL,
        y1 integer NOT NULL,
        x2 integer NOT NULL,
        y2 integer NOT NULL,
        entity text NOT NULL,
        is_all_columns boolean DEFAULT false NOT NULL,
        column_refs jsonb
      )`);

    console.log('ectomigo database initialized.');

    process.exit(0);
  });

program
  .command('scan <dir>')
  .option('-r, --repo [name]', 'name this repo (default: directory name)')
  .option('-i, --ignore <glob...>', 'ignore path globs such as migration script directories', (val, acc) => acc.concat([val]), [])
  .option('-p, --patterns <name:glob...>', 'map named data access patterns to path globs, e.g. "massive:lib/**/*"', (val, acc) => {
    const [name, glob] = val.split(':');

    if (!acc[name]) {
      acc[name] = [];
    }

    acc[name].push(glob);

    return acc;
  }, {})
  .description('index data access code in a local repository/directory')
  .addHelpText('after', `
if the repo already exists (check with the "ls" subcommand) and has ignore
paths or patterns defined, it will continue to use them in the absence of
explicit --ignore or --patterns directives.

available patterns:

  * massive (MassiveJS data mapper)
  * sqlalchemy (Python SQL toolkit and O/RM)`)
  .action(async function (dir, options) {
    const db = await massive(DB_NAME);
    const absolutePath = resolve(dir);
    const name = options.repo || basename(dir);
    let repo = await db.repos.findOne({name: name});

    if (!repo) {
      repo = {name};
    }

    if (repo.ignore_paths && _.isEmpty(options.ignore)) {
      console.log(`using existing ignore paths definition: ${JSON.stringify(repo.ignore_paths)}`);
    } else if (!_.isEmpty(options.ignore)) {
      repo.ignore_paths = options.ignore;
    }

    if (repo.patterns && _.isEmpty(options.patterns)) {
      console.log(`using existing patterns definition: ${JSON.stringify(repo.patterns)}`);
    } else if (!_.isEmpty(options.patterns)) {
      repo.patterns = options.patterns;
    }

    repo.url = absolutePath;
    repo.scanned_at = new Date();

    repo = await db.repos.save(repo);

    await db.invocations.destroy({
      repo_id: repo.repo_id
    });

    const files = await globby(
      _.concat(
        // all files....
        [`${absolutePath}/**/*`],
        // ....except anything in ignore paths
        _.concat(
          DEFAULT_IGNORE,
          options.ignore || []
        ).map(p => `!${absolutePath}/**/${p}`)
      ),
      {
        gitignore: true,
        cwd: absolutePath
      }
    );

    let count = 0;

    await pipeline(
      Readable.from(indexFiles(repo, dir, files)),
      Writable({
        objectMode: true,
        write: function (inv, enc, next) {
          return db.invocations.insert({
            repo_id: repo.repo_id,
            file_path: inv.file_path,
            x1: inv.x1,
            y1: inv.y1,
            x2: inv.x2,
            y2: inv.y2,
            entity: inv.entity,
            confidence: inv.confidence || 1, // only patterns set <1 confidence
            is_all_columns: inv.is_all_columns || false,
            column_refs: JSON.stringify(inv.column_refs)
          }).then(() => {
            count++;

            next();
          });
        }
      })
    );

    console.log(`indexed ${count} database invocations in ${name}`);

    process.exit(0);
  });

program
  .command('check <entity>')
  .description('locate uses of a database entity')
  .action(async function (entity) {
    const db = await massive(DB_NAME);

    const invocations = await db.invocations.join({
      repo: {
        relation: 'repos',
        decomposeTo: 'object'
      }
    }).find({
      entity: entity
    });

    const byRepo = _.groupBy(invocations, i => i.repo.name);
    let repoCount = 0;

    for (const repo in byRepo) {
      console.log(colors.brightGreen.bold(`references to ${entity} in ${repo} (${byRepo[repo][0].repo.url}):`))
      console.log();

      const byFile = _.groupBy(byRepo[repo], i => i.file_path);

      for (const file of Object.keys(byFile).sort()) {
        console.log(colors.brightCyan(`${file}:`));

        for (const invocation of byFile[file]) {
          let str = `  line ${invocation.y1}`

          if (invocation.is_all_columns) {
            str += ': all columns'
          } else if (invocation.column_refs) {
            const refs = invocation.column_refs.sort((a, b) => b.confidence - a.confidence);

            str += `: ${refs.map(r => r.name).join(', ')}`
          }

          console.log(str);
        }
      }

      console.log();

      repoCount++;
    }

    console.log(colors.brightGreen(`ectomigo found ${invocations.length} invocations in ${repoCount} ${repoCount === 1 ? 'repository' : 'repositories'}`));

    process.exit(0);
  });

program
  .command('ls')
  .description('list indexed repos')
  .action(async function () {
    const db = await massive(DB_NAME);
    const repos = await db.repos.find({}, {
      order: [{field: 'name'}]
    });

    const headers = ['name', 'path', 'scanned'];
    const widths = headers.map(h => h.length + 4);

    const items = repos.reduce((acc, repo) => {
      const scannedAt = repo.scanned_at.toLocaleString();
      const row = [repo.name, repo.url, scannedAt];

      widths[0] = Math.max(widths[0], repo.name.length + 4);
      widths[1] = Math.max(widths[1], repo.url.length + 4);
      widths[2] = Math.max(widths[2], scannedAt.length + 4);

      acc.push(row);

      return acc;
    }, []);

    console.log(
      colors.brightGreen.underline(headers.map((h, idx) => h.padStart(widths[idx])).join(' '))
    )

    for (const row of items) {
      console.log(
        colors.brightGreen(row[0].padStart(widths[0])),
        colors.brightWhite(row[1].padStart(widths[1])),
        colors.brightCyan(row[2].padStart(widths[2]))
      );
    }

    process.exit(0);
  });

program
  .command('info <name>')
  .description('show more info on a scanned repo')
  .action(async function (name) {
    const db = await massive(DB_NAME);

    const repo = await db.repos.findOne({name}, {
      order: [{field: 'name'}]
    });

    const widths = [0];
    const rows = ['name', 'url', 'ignore_paths', 'patterns', 'created_at', 'scanned_at'].reduce((acc, f) => {
      let val = repo[f];

      if (!val) {
        return acc;
      } else if (val instanceof Date) {
        val = val.toLocaleString();
      } else if (_.isArray(val)) {
        val = JSON.stringify(val);
      } else if (_.isObject(val)) {
        val = JSON.stringify(val);
      }

      const row = [f, val];

      widths[0] = Math.max(widths[0], f.length);

      acc.push(row);

      return acc;
    }, []);

    for (const row of rows) {
      console.log(
        colors.brightGreen(row[0].padStart(widths[0])),
        colors.brightWhite(row[1]),
      );
    }

    process.exit(0);
  });

program
  .command('rm <name>')
  .description('delete a repo\'s scan results')
  .action(async function (name) {
    const db = await massive(DB_NAME);

    const repo = await db.repos.findOne({name});

    if (repo) {
      await db.repos.destroy(repo.repo_id);

      console.log(`cleaned ${name}.`);
    } else {
      console.log(`no repo ${name}, nothing to do.`);
    }

    process.exit(0);
  })

program.parse(process.argv);
